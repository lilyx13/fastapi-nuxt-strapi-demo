from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

# Testing allow everything
app.add_middleware(
  CORSMiddleware,
  allow_credentials=True,
  allow_origins=["*"],
  allow_headers=["*"],
)

@app.get("/")
async def home():
  return { "welcome": "This is a welcome message from FastAPI."}

@app.get("/hello")
async def hello():
  return { "message": "Hello World"}