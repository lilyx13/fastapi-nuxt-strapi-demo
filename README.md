# README

This is a test implementation of Fast Api paired with Nuxt 3 and the Strapi Headless CMS.

- Client = Nuxt 3
- Server = Fast Api
- Data = Strapi v4

## Setup
General walkthrough of how to set this up and run it.

### Server
- [Fast Api Docs](https://fastapi.tiangolo.com/)

#### Steps
1. Set up ENV
  - `python -m venv env`
  - `. env/bin/activate`
2. Install packages
  - `pip install fastapi`
  - `pip install "uvicorn[standard]"`
3. Add routes to `main.py`
4. Run live Server
  - `uvicorn main:app --reload`