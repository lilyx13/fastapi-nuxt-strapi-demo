# README - Utility Components
- Small presentational components
- Similar to UI components, but for smaller use cases
  - ie: a general unordered list vs the more specific AppList in `ui components`